# Approch for solution.
NOTE: I went out to shave that Yak, but I am happy about the setup that I have :)

## TODO
- Create initial setup for project with typescript prettier etc, decide which framework to use without spending much time
- Create typesript models for links
- Create  REST API for  project
- Add validation schema on routes, decide joi or AJV.
- Find or create error handling middleware.

## NPM Script?
- Make sure you have nodejs installed (Nodejs > v10)
- Project is self explainatory if you see `npm script`
- `npm run dev:start` will start project in dev mode using nodemon and ts-node
- `npm run build` will only compile project from ts to js.
- `npm run start` will compile and run project
- `npm run lint` and `npm run lint:fix`  is for linting purpose
- `npm run test` will run test cases but I havent added many due to time contraint, but project is setup up with production grade jest setup.

## How to run project?
- `npm install`
- `npm run dev:start` or if you are using vscode then vscode launch file is added if like to start project in debug mode.
- There is `api` folder at root of project, you can use postman api list.
- Thats all we need to interact with project

## Note
- Project only have 2 API, one GET links by userId API (with sorting included) and POST user link (single api handle classic, music and show)
- Project has simple mock data file and when running project we interact with inmemory data.
- In `models` folder typescript type model included, one can use this folder to add DB models/ ORM files
- We also have `modules` foder to seperate modules like users, links, orders, payments, etc but here we only have `link-module`
- Each module have seperate controller file, request validation schema file, and service file.
- In `Utils` folder, we can keep general util function, middlerware etc.

---
## Personal Notes
- Initialy spend time on modeling data, 1st think in project I did is 1) Create typs for data and 2) created Mock data.
- I can not image writting code without typescript, prettier, eslint, on save format, this is where I spend quite a time
- I havent use expressjs in last 2-3 years, but I looked into other framework like fastify, nextjs but they were doing tomany things and I dont want to read docs for simple project.
- But When started with Expressjs found that there is no one specific answer to how to request validation or how to throw and handle errors properly, also express js dont handle async await out of the box.
- So finally endup writting my own middlewares and async handler for express controller (I quite like final result.)





---
<p align="center">
  <img src="https://github.com/blstrco/linktr.ee-backend-assessment/raw/master/Screen%20Shot%202019-07-08%20at%202.09.47%20pm.png">
</p>

# The Problem
We have three new link types for our users.

1. Classic
	- Titles can be no longer than 144 characters.
	- Some URLs will contain query parameters, some will not.
2. Shows List
	- One show will be sold out.
	- One show is not yet on sale.
	- The rest of the shows are on sale.
3. Music Player
	- Clients will need to link off to each individual platform.
	- Clients will embed audio players from each individual platform.
	
You are required to create a JSON API that our front end clients will interact with.

- The API can be GraphQL or REST.
- The API can be written in your preferred language.
- The client must be able to create a new link of each type.
- The client must be able to find all links matching a particular userId.
- The client must be able to find links matching a particular userId, sorted by dateCreated.


## Your Solution

- Consider bad input data and the end user of your API - we're looking for good error handling and input validation.
- If you are creating a GraphQL API, think about the access patterns the client may use, and think about the acces patterns the client may not use. Try not to [Yak Shave](https://seths.blog/2005/03/dont_shave_that/)
- Consider extensibility, these are 3 of hundreds of potential link types that we will be developing.


## Rules & Tips

- Choose the language and environment of your choice, just include documentation on how to run your code.
- Immutability and functional programming is looked upon favorably.
- You cannot connect to a real world database - document your schema design.
- Mocking third parties is looked upon favorably.
- @todo comments are encouraged. You aren't expected to complete the challenge, but how you design your solution and your ideas for the future are important.

---
# Submission
Set up your own remote git repository and make commits as you would in your day to day work. Submit a link to your repo when you're finished.
---

