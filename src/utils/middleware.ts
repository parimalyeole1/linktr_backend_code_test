import { Request, Response, NextFunction, RequestHandler, ErrorRequestHandler } from 'express';
import { ValidationError } from 'express-json-validator-middleware';
import createHttpError from 'http-errors';
import createError from 'http-errors';

type asyncFn = (req: Request) => Promise<any>;
export const ah = (fn: asyncFn): RequestHandler =>
  async function asyncUtilWrap(req: Request, res: Response, next: NextFunction) {
    try {
      const data = await fn(req);
      res.send({ success: true, error: {}, data });
    } catch (error) {
      if (error instanceof ValidationError) {
        // Handle the error
        res.status(400).send({ success: false, error: error.validationErrors, data: {} });
        // next();
      } else {
        // Pass error on if not a validation error
        next(error);
      }
    }
  };

export const validationErrorMiddleware: ErrorRequestHandler = (
  error: ValidationError | Error | createHttpError.CreateHttpError,
  req,
  res,
  next,
) => {
  console.info('Error Handling Middleware called');
  console.info(`Route: ${req.method} => ${req.path}`);
  console.error('Error: ', error);
  if (error instanceof ValidationError) {
    // Handler error from validation schema
    res.status(400).send({ success: false, error: error.validationErrors, data: {} });
  } else if (createError.isHttpError(error)) {
    // Handler error thrown from controller with createError
    res.status(error.status).send({ success: false, error: error.message, data: {} });
  } else {
    // Handler unknown error
    res.status(500).send({ success: false, error: 'Something went wrong!', data: {} });
  }
  next();
};
