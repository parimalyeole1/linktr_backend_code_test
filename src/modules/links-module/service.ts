import createError from 'http-errors';
import { filter, find, findIndex, orderBy } from 'lodash';
import { userLinksDb } from '../../mock.data';
import { LinkEnum, LinkType, sortEnum, UserLink } from '../../models';

// NOTE: all the services methods are async as this will be db call in actual life
export const getLinksByUserId = async (userId: string, sortByDate: sortEnum): Promise<UserLink[]> => {
  return orderBy(
    filter(userLinksDb, (l) => l.userId === userId),
    (o) => new Date(o.createdAt),
    [sortByDate],
  );
};

export const saveLink = async (userId: string, link: LinkType): Promise<UserLink> => {
  const foundUserLink = find(
    userLinksDb,
    (ul) => ul.userId === userId && ul.link.type === link.type && ul.link.title === link.title,
  );
  if (!foundUserLink) {
    // create
    const newLinkId = `${10000 + userLinksDb.length + 1}`;
    const userLink: UserLink = {
      id: newLinkId,
      userId: userId,
      link: link,
      createdAt: new Date(),
    };
    userLinksDb.push(userLink);
    return userLink;
  }
  // update
  if (link.type === LinkEnum.CLASSIC) {
    throw createError(400, `${LinkEnum.CLASSIC} link with title ${link.title} already exist!`);
  }
  if (link.type === LinkEnum.SHOW && foundUserLink.link.type === LinkEnum.SHOW) {
    foundUserLink.link.shows = [...foundUserLink.link.shows, ...link.shows];
  }
  if (link.type === LinkEnum.MUSIC && foundUserLink.link.type === LinkEnum.MUSIC) {
    foundUserLink.link.platforms = [...foundUserLink.link.platforms, ...link.platforms];
  }
  // find replace item
  userLinksDb[findIndex(userLinksDb, (o) => o.id == foundUserLink.id)] = foundUserLink;
  return foundUserLink;
};
