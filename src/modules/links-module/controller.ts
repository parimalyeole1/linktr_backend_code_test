import { Request } from 'express';
import createError from 'http-errors';
import { includes } from 'lodash';
import { usersList } from '../../mock.data';
import { sortEnum, UserLink } from '../../models';
import { getLinksByUserId, saveLink } from './service';

export const postUserLinkController = async (req: Request): Promise<UserLink> => {
  const userId = req.params.userId;
  if (!includes(usersList, userId)) {
    throw createError(400, 'User not found.');
  }
  return saveLink(userId, req.body);
};

export const listuserLinkController = async (req: Request): Promise<UserLink[]> => {
  const userId = req.params.userId;
  if (!includes(usersList, userId)) {
    // NOTE: there is better way to validate user existance throw JWT, this is just the way to show how to throw error properly from controller
    throw createError(400, 'User not found.');
  }
  const sortByDate = req.query.sortByDate as unknown as sortEnum;
  const userLinks = await getLinksByUserId(userId, sortByDate);
  return userLinks;
};
