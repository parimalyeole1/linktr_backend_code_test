import { LinkEnum, musicPlatformEnumArr, showStatusArr, sortEnum, sortEnumArray } from '../../models';
export const userIdParamsSchema = {
  type: 'object',
  required: ['userId'],
  properties: {
    userId: {
      type: 'string',
      minLength: 10,
    },
  },
} as const;

export const sortByDateQuerySchema = {
  type: 'object',
  properties: {
    sortByDate: {
      type: 'string',
      default: sortEnum.ASC,
      enum: sortEnumArray,
    },
  },
} as const;

export const linkBodySchema = {
  type: 'object',
  anyOf: [
    {
      required: ['type', 'title', 'link'],
      properties: {
        type: { type: 'string', enum: [LinkEnum.CLASSIC] },
        title: { type: 'string', maxLength: 144 },
        link: { type: 'string', minLeangth: 5 },
      },
    },
    {
      required: ['type', 'title', 'shows'],
      properties: {
        type: { type: 'string', enum: [LinkEnum.SHOW] },
        title: { type: 'string', maxLength: 144 },
        shows: {
          type: 'array',
          minItems: 1,
          items: {
            type: 'object',
            required: ['status', 'link'],
            properties: {
              status: { type: 'string', enum: showStatusArr },
              link: { type: 'string', minLeangth: 5 },
            },
          },
        },
      },
    },
    {
      required: ['type', 'title', 'platforms'],
      properties: {
        type: { type: 'string', enum: [LinkEnum.MUSIC] },
        title: { type: 'string', maxLength: 144 },
        platforms: {
          type: 'array',
          minItems: 1,
          items: {
            type: 'object',
            required: ['type', 'link', 'iframeLink'],
            properties: {
              type: { type: 'string', enum: musicPlatformEnumArr },
              link: { type: 'string', minLeangth: 5 },
              iframeLink: { type: 'string', minLeangth: 5 },
            },
          },
        },
      },
    },
  ],
} as const;
