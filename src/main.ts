import express, { Application } from 'express';
import { Validator } from 'express-json-validator-middleware';
import { listuserLinkController, postUserLinkController } from './modules/links-module/controller';
import { linkBodySchema, sortByDateQuerySchema, userIdParamsSchema } from './modules/links-module/validation.schema';
import { ah, validationErrorMiddleware } from './utils';

const { validate } = new Validator({
  coerceTypes: 'array',
  allErrors: true,
  useDefaults: 'empty',
  messages: false,
});
const app: Application = express();
const port = 5000;

app.use(express.json());

// Route: Link Module
// post user link
app.post(
  '/users/:userId/links',
  validate(<any>{ params: userIdParamsSchema, body: linkBodySchema }),
  ah(postUserLinkController),
);
// list user link
app.get(
  '/users/:userId/links',
  validate(<any>{ params: userIdParamsSchema, query: sortByDateQuerySchema }),
  ah(listuserLinkController),
);

app.use(validationErrorMiddleware);

// Start server
app.listen(port, () => console.log(`Server is listening on port ${port}!`));
