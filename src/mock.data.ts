import { LinkEnum, MusicPlatformEnum, ShowStatus, UserLink } from './models/links.types';

export const usersList: string[] = ['user-10001', 'user-10002', 'user-10003', 'user-10004'];
// NOTE: Can store multiple user data but initial mock data will be only for 2 user
export const userLinksDb: UserLink[] = [
  // user-123
  {
    id: '10001',
    userId: 'user-10001',
    link: {
      type: LinkEnum.CLASSIC,
      title: 'Facebook',
      link: 'https://www.facebook.com/user-123',
    },
    createdAt: new Date('2021-08-22T14:47:33.902Z'),
  },
  {
    id: '10002',
    userId: 'user-10001',
    link: {
      type: LinkEnum.CLASSIC,
      title: 'Instagram',
      link: 'https://www.instagram.com/user-123',
    },
    createdAt: new Date('2021-08-21T14:47:33.902Z'),
  },
  {
    id: '10003',
    userId: 'user-10001',
    link: {
      type: LinkEnum.MUSIC,
      title: 'Marron 5 Cover',
      platforms: [
        {
          type: MusicPlatformEnum.SPOTIFY,
          link: 'https://open.spotify.com/track/5d6Mjuu2uCGRPYpFjGpCX5?si=4af22039fd294db3',
          iframeLink:
            '<iframe src="https://open.spotify.com/embed/track/5d6Mjuu2uCGRPYpFjGpCX5" width="100%" height="80" frameBorder="0" allowtransparency="true" allow="encrypted-media"></iframe>',
        },
        {
          type: MusicPlatformEnum.SOUNDCLOUD,
          link: 'https://soundcloud.com/interscope/maroon-5-sugar',
          iframeLink:
            '<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/166300320&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/interscope" title="Interscope Records" target="_blank" style="color: #cccccc; text-decoration: none;">Interscope Records</a> · <a href="https://soundcloud.com/interscope/maroon-5-sugar" title="Maroon 5 - Sugar" target="_blank" style="color: #cccccc; text-decoration: none;">Maroon 5 - Sugar</a></div>',
        },
        {
          type: MusicPlatformEnum.YOUTUBE_MUSIC,
          link: 'https://johannahmontanah.bandcamp.com/track/sugar-maroon-5-cover',
          iframeLink:
            '<iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=1584654685/size=small/bgcol=333333/linkcol=0f91ff/track=2469384187/transparent=true/" seamless><a href="https://johannahmontanah.bandcamp.com/album/mary-jane-sugar">Mary Jane / Sugar by JohannahMontanah</a></iframe>',
        },
      ],
    },
    createdAt: new Date('2021-08-23T14:47:33.902Z'),
  },
  {
    id: '10004',
    userId: 'user-10001',
    link: {
      type: LinkEnum.SHOW,
      title: 'Weight Loss Comedy',
      shows: [
        {
          link: 'https://in.bookmyshow.com/events/that-comedy-club-bandra/ET00146021',
          status: ShowStatus.NOT_ON_SALE,
        },
        {
          link: 'https://in.bookmyshow.com/events/that-comedy-club-bandra/ET00146021',
          status: ShowStatus.SOLD_OUT,
        },
        {
          link: 'https://in.bookmyshow.com/events/that-comedy-club-bandra/ET00146021',
          status: ShowStatus.ON_SALE,
        },
      ],
    },
    createdAt: new Date('2021-07-18T14:47:33.902Z'),
  },
  // user-10002
  {
    id: '10001',
    userId: 'user-10002',
    link: {
      type: LinkEnum.CLASSIC,
      title: 'Facebook',
      link: 'https://www.facebook.com/user-123',
    },
    createdAt: new Date('2021-06-23T14:47:33.902Z'),
  },
  {
    id: '10002',
    userId: 'user-10002',
    link: {
      type: LinkEnum.CLASSIC,
      title: 'Instagram',
      link: 'https://www.instagram.com/user-123',
    },
    createdAt: new Date('2021-05-23T14:47:33.902Z'),
  },
  {
    id: '10003',
    userId: 'user-10002',
    link: {
      type: LinkEnum.MUSIC,
      title: 'Marron 5 Cover',
      platforms: [
        {
          type: MusicPlatformEnum.SPOTIFY,
          link: 'https://open.spotify.com/track/5d6Mjuu2uCGRPYpFjGpCX5?si=4af22039fd294db3',
          iframeLink:
            '<iframe src="https://open.spotify.com/embed/track/5d6Mjuu2uCGRPYpFjGpCX5" width="100%" height="80" frameBorder="0" allowtransparency="true" allow="encrypted-media"></iframe>',
        },
        {
          type: MusicPlatformEnum.SOUNDCLOUD,
          link: 'https://soundcloud.com/interscope/maroon-5-sugar',
          iframeLink:
            '<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/166300320&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/interscope" title="Interscope Records" target="_blank" style="color: #cccccc; text-decoration: none;">Interscope Records</a> · <a href="https://soundcloud.com/interscope/maroon-5-sugar" title="Maroon 5 - Sugar" target="_blank" style="color: #cccccc; text-decoration: none;">Maroon 5 - Sugar</a></div>',
        },
        {
          type: MusicPlatformEnum.YOUTUBE_MUSIC,
          link: 'https://johannahmontanah.bandcamp.com/track/sugar-maroon-5-cover',
          iframeLink:
            '<iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=1584654685/size=small/bgcol=333333/linkcol=0f91ff/track=2469384187/transparent=true/" seamless><a href="https://johannahmontanah.bandcamp.com/album/mary-jane-sugar">Mary Jane / Sugar by JohannahMontanah</a></iframe>',
        },
      ],
    },
    createdAt: new Date('2021-06-12T14:47:33.902Z'),
  },
  {
    id: '10004',
    userId: 'user-10002',
    link: {
      type: LinkEnum.SHOW,
      title: 'Weight Loss Comedy',
      shows: [
        {
          link: 'https://in.bookmyshow.com/events/that-comedy-club-bandra/ET00146021',
          status: ShowStatus.NOT_ON_SALE,
        },
        {
          link: 'https://in.bookmyshow.com/events/that-comedy-club-bandra/ET00146021',
          status: ShowStatus.SOLD_OUT,
        },
        {
          link: 'https://in.bookmyshow.com/events/that-comedy-club-bandra/ET00146021',
          status: ShowStatus.ON_SALE,
        },
      ],
    },
    createdAt: new Date('2021-02-23T14:47:33.902Z'),
  },
];
