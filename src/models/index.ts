export * from './links.types';

export enum sortEnum {
  DESC = 'desc',
  ASC = 'asc',
}

export const sortEnumArray = [sortEnum.ASC, sortEnum.DESC];
