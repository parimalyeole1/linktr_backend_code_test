export enum LinkEnum {
  CLASSIC = 'classic',
  MUSIC = 'music',
  SHOW = 'show',
}

export const LinkEnumArr = [LinkEnum.CLASSIC, LinkEnum.MUSIC, LinkEnum.SHOW];

export interface Link {
  type: LinkEnum.CLASSIC | LinkEnum.MUSIC | LinkEnum.SHOW;
  title: string;
}

export interface ClassicLink extends Link {
  type: LinkEnum.CLASSIC;
  link: string;
}

export enum MusicPlatformEnum {
  SPOTIFY = 'spotify',
  APPLE_MUSIC = 'apple_music',
  YOUTUBE_MUSIC = 'yoututbe_music',
  SOUNDCLOUD = 'soundcloud',
}
export const musicPlatformEnumArr = [
  MusicPlatformEnum.SPOTIFY,
  MusicPlatformEnum.APPLE_MUSIC,
  MusicPlatformEnum.YOUTUBE_MUSIC,
  MusicPlatformEnum.SOUNDCLOUD,
];
export interface MusicPlatform {
  type: MusicPlatformEnum;
  link: string;
  iframeLink: string;
}
export interface MusicLink extends Link {
  type: LinkEnum.MUSIC;
  platforms: MusicPlatform[];
}

export enum ShowStatus {
  NOT_ON_SALE = 'not_on_sale',
  ON_SALE = 'on_sale',
  SOLD_OUT = 'sold_out',
}
export const showStatusArr = [ShowStatus.NOT_ON_SALE, ShowStatus.ON_SALE, ShowStatus.SOLD_OUT];
export interface Show {
  status: ShowStatus;
  link: string;
}
export interface ShowLink extends Link {
  type: LinkEnum.SHOW;
  shows: Show[];
}

export type LinkType = ClassicLink | MusicLink | ShowLink;

export interface UserLink {
  id: string;
  userId: string;
  link: LinkType;
  createdAt: Date;
}
